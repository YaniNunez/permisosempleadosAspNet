﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Permiso.aspx.cs" Inherits="Examen.Web.Views.Edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h1>Permisos empleados</h1>
        <p id="tituloEditar" class="lead">Editar permiso</p>
         <p id="tituloNuevo" class="lead">Nuevo permiso </p>
    </div>
    <div class="container">
        <h3>Datos de Empleado</h3>
        <hr />
        <form id="form_id">
            <input id="id" type="hidden" />
            <div class="form-group">
            <label class="control-label" for="nombreEmpleado">Nombre</label>
            <input type="text" class="form-control" id="nombreEmpleado" aria-describedby="helpBlockNombre" required>


        </div>
        <div class="form-group">
            <label class="control-label" for="apellidoEmpleado">Apellido</label>
            <input type="text" class="form-control" id="apellidoEmpleado" aria-describedby="helpBlockApellido" required>
          
        </div>
        <div class="form-group">
            <label class="control-label" for="tipoPermiso">Tipo Permiso</label>
           <select id="tipoPermiso" class="form-control" required></select>
            
        </div>
          <button type="submit" class="btn btn-primary" >Grabar</button>
        </form>
        
        
    </div>
    <script>
        getTipoPermisos();
        setPage();
        function getTipoPermisos() {
            $.ajax({
                type: "GET",
                url: "/api/TipoPermisos/GetAllTipoPermisosAsync",
                success: function (data) {
                   
                    $(data).each(function () {
                        var option = $(document.createElement('option'));
                        option.text(this.Descrpcion);
                        option.val(this.Id);
                        $("#tipoPermiso").append(option);
                    });
                }
            });
        }
        function setPage() {
            var url = window.location.pathname;
            var id = url.substring(url.lastIndexOf('/') + 1);
           
            if ($.isNumeric(id)==true ) {
                $("#tituloEditar").show();
                $("#tituloNuevo").hide();
                getEmpleadoById(id);

            } else {
                $("#tituloEditar").hide();
                $("#tituloNuevo").show();

            }

        }
        function getEmpleadoById(id) {
            $.ajax({
                type: "GET",
                url: "/api/Permisos/"+id,
                success: function (data) {
                    $("#nombreEmpleado").val(data.NombreEmpleado);
                    $("#apellidoEmpleado").val(data.ApellidosEmpleado);
                    $("#tipoPermiso").val(data.TipoPermisoId);
                    $("#tipoPermiso option[value='" + data.TipoPermisoId + "']").attr("selected", true);
                    $("#id").val(data.$id);
                }
            });
        }
        $('form').on('submit', function (e) {
          
            e.preventDefault();
           
                let permiso = {
                    NombreEmpleado: $("#nombreEmpleado").val(),
                    ApellidosEmpleado: $("#apellidoEmpleado").val(),
                    TipoPermisoId: $("#tipoPermiso").val(),
                    Id: $("#id").val()
                }
                let accion = "POST";
                if (permiso.Id != "") {
                    accion = "PUT";
                }
                $.ajax({
                    type: accion,
                    url: "/api/Permisos",
                    data: permiso,
                    success: function (data) {
                        alert("Se guardó correctamente");
                        window.location.href = "/Default.aspx";
                    }
                });
            


        });
    </script>
</asp:Content>
