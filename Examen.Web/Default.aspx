﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Examen.Web._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

  <div class="jumbotron">
    <h1>Permisos empleados</h1>
    <p class="lead">Administración de permisos de empleados</p>   
</div>
<div class="row">
    <div class="col-md-12">
     <table class="table table-hover">
         <thead>
             <th>Nombre</th>
             <th>Apellido</th>
             <th>Tipo de Permiso</th>
             <th>Fecha</th>
             <th><a id="new" class="btn btn-success" href="/Permiso">Nuevo</a></th>
         </thead>
         <tbody id="rows">
            
         </tbody>
     </table>
    </div>

  <div class="modal fade" tabindex="-1" role="dialog" id="deleteModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Eliminar</h4>
      </div>
      <div class="modal-body">
        <p>Desea continuar eliminando este permiso?</p>
          <input id="deleteId" type="hidden"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="deletePermiso">Aceptar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

   
</div>
    <script>
        getListPermisos();
        function getListPermisos() {
            $.ajax({
                type: "GET",
                url: "/api/Permisos",                
                success: function (data) {
                    $("#rows").html("");
                    $(data).each(function (i,obj) {
                        let tr = $("<tr id='"+obj.$id+"'></tr>");
                        $(tr).append("<td>" + obj.NombreEmpleado+"</td>")
                        $(tr).append("<td>" + obj.ApellidosEmpleado + "</td>")
                        $(tr).append("<td>" + obj.TipoPermiso + "</td>")
                        $(tr).append("<td>" + obj.FechaPermiso + "</td>")
                        let botones = $('<div class="btn-group btn-xs" role="group" aria-label="..."><a href="/Permiso/' + obj.$id +'" class= "btn btn-primary Edit"> Editar</a><button type="button" class="btn btn-info" id="delete_' + obj.$id +'">Eliminar</button></div >');
                        $(tr).append("<td></td>").append($(botones));                      
                        $("#rows").append($(tr));

                        $("#delete_" + obj.$id).on("click", function (e) { 
                            $("#deleteId").val(obj.$id);
                            $("#deleteModal").modal("show");

                        });
                    });
                    
                    
                }
            });
            $("#deletePermiso").on("click", function () {
                $.ajax({

                    type: "DELETE",
                    url: "/api/Permisos/" + $("#deleteId").val(),
                    success: function (data) {
                        alert("Se elimino correctamente");
                        $("#deleteModal").modal("hide");
                        getListPermisos();

                    }
                });

            });
            
        }
      
    </script>
</asp:Content>
