﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Examen.Web.ViewModels
{
    public class PermisoViewModel
    {
        public int Id { get; set; }
        public string NombreEmpleado { get; set; }
        public string ApellidosEmpleado { get; set; }
        public string FechaPermiso { get; set; }
        public string TipoPermiso  { get; set; }
    }
}