﻿using Examen.Data;
using Examen.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Examen.Web.ViewModels;
using System.Linq;

namespace Examen.Web.Controllers
{
    public class PermisosController : ApiController
    {
        private readonly IPermisoService _permisoService;
        private readonly ITipoPermisosService _tipoPermisosService;
        private readonly EmpresaContext _db;
       
        public PermisosController()
        {
            _db = new EmpresaContext();
            _permisoService =new PermisoService(_db) ;
            _tipoPermisosService = new TipoPermisosService(_db);
        }

        [HttpGet]
        
        public IList<PermisoViewModel> GetAllAsync()
        {
            try
            {
                return  _permisoService.List().Select(p=> new PermisoViewModel
                {
                    NombreEmpleado = p.NombreEmpleado,
                    ApellidosEmpleado = p.ApellidosEmpleado,
                    FechaPermiso = p.FechaPermiso.ToShortDateString(),
                    TipoPermiso = p.TipoPermiso.Descrpcion,
                    Id = p.Id
                }).ToList();
               
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Se produjo un error, detalles: ", ex.Message);
            }


        }
       
        [HttpGet]
        [Route("api/permisos/{id:int}")]
        public Permiso GetByIdAsync(int id)
        {
            try
            {
                return _permisoService.GetById(id);

            }
            catch (Exception ex)
            {

                throw new ArgumentException("Se produjo un error, detalles: ", ex.Message);
            }

        }
        
        [HttpPost]
      
        public async Task Add(Permiso permiso)
        {
            try
            {
                await _permisoService.Add(permiso);
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Se produjo un error, detalles: ", ex.Message);
            }

        }
        [HttpPut]
        public async Task Edit(Permiso permiso)
        {
            try
            {
                await _permisoService.Edit(permiso);
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Se produjo un error, detalles: ", ex.Message);
            }

        }
        [HttpDelete]
        public async Task Delete(int id)
        {
            try
            {
                await _permisoService.Delete(id);
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Se produjo un error, detalles: ", ex.Message);
            }

        }
    }
}