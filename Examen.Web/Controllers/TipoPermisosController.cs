﻿using Examen.Data;
using Examen.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Examen.Web.ViewModels;
using System.Linq;

namespace Examen.Web.Controllers
{
    public class TipoPermisosController : ApiController
    {
       
        private readonly ITipoPermisosService _tipoPermisosService;
        private readonly EmpresaContext _db;
       
        public TipoPermisosController()
        {
            _db = new EmpresaContext();
           
            _tipoPermisosService = new TipoPermisosService(_db);
        }

     
        [HttpGet]
        public IList<TipoPermiso> GetAllAsync()
        {
            try
            {
                return _tipoPermisosService.List();

            }
            catch (Exception ex)
            {

                throw new ArgumentException("Se produjo un error, detalles: ", ex.Message);
            }

        }
       
    }
}