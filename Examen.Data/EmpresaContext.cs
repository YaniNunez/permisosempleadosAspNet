namespace Examen.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class EmpresaContext : DbContext
    {
        public EmpresaContext()
            : base("name=EmpresaContext")
        {
        }
        public virtual DbSet<Permiso> Permiso { get; set; }
        public virtual DbSet<TipoPermiso> TipoPermiso { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           

            modelBuilder.Entity<Permiso>()
                .Property(e => e.NombreEmpleado)
                .IsUnicode(false);

            modelBuilder.Entity<Permiso>()
                .Property(e => e.ApellidosEmpleado)
                .IsUnicode(false);

            modelBuilder.Entity<TipoPermiso>()
                .Property(e => e.Descrpcion)
                .IsUnicode(false);

            modelBuilder.Entity<TipoPermiso>()
                .HasMany(e => e.Permiso)
                .WithRequired(e => e.TipoPermiso)
                .WillCascadeOnDelete(false);
        }
    }
}
