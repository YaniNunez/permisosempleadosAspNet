﻿using Examen.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen.Services.Interfaces
{
    public class TipoPermisosService : ITipoPermisosService
    {
        private readonly EmpresaContext _context;

        public TipoPermisosService(EmpresaContext context)
        {
            _context = context;
        }

       
        public IList<TipoPermiso> List()
        {
            try
            {
                return _context.TipoPermiso.ToList<TipoPermiso>();
            }
            catch (Exception ex)
            {

                throw ex;
            }
           

        }

    }
}




