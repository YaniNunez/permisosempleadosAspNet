﻿using Examen.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen.Services.Interfaces
{
    public class PermisoService : IPermisoService
    {
        private readonly EmpresaContext _context;

        public PermisoService(EmpresaContext context)
        {
            _context = context;
        }
      
        public async Task Add(Permiso permiso)
        {

            if (!string.IsNullOrEmpty(permiso.ApellidosEmpleado) &&
                !string.IsNullOrEmpty(permiso.NombreEmpleado))
            {
               
                _context.Permiso.Add(permiso);
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new ArgumentException("Falta completar el nombre o apellido.");
            }

        }
        public IList<Permiso> List()
        {
            try
            {
                return _context.Permiso.ToList<Permiso>();
            }
            catch (Exception ex)
            {

                throw ex;
            }
           

        }
        public async Task Delete(int id)
        {
            var permiso = await _context.Permiso.FindAsync(id);
            _context.Permiso.Remove(permiso);
            await _context.SaveChangesAsync();
        }

        public async Task Edit(Permiso permiso)
        {

            if (!string.IsNullOrEmpty(permiso.ApellidosEmpleado) &&
               !string.IsNullOrEmpty(permiso.NombreEmpleado))
            {
                if (!string.IsNullOrEmpty(permiso.ApellidosEmpleado) &&
                      !string.IsNullOrEmpty(permiso.NombreEmpleado))
                {
                    var permisoById = _context.Permiso.FirstOrDefault(p => p.Id == permiso.Id);
                    permisoById.NombreEmpleado = permiso.NombreEmpleado;
                    permisoById.ApellidosEmpleado = permiso.ApellidosEmpleado;
                    permisoById.TipoPermisoId = permiso.TipoPermisoId;
                    permisoById.FechaPermiso = permiso.FechaPermiso;
                    await _context.SaveChangesAsync();
                }
                else
                {
                    throw new ArgumentException("Falta completar el nombre o apellido.");
                }
            }
        }

        public Permiso GetById(int id)
        {
            try
            {

                return _context.Permiso.Where(p => p.Id == id).FirstOrDefault();
            }
            catch (Exception ex )
            {

                throw ex;
            }
        }
    }
}




