﻿using Examen.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen.Services.Interfaces
{
    public interface IPermisoService
    {
        IList<Permiso> List();
         Task Add(Permiso permiso);
         Task Delete(int id);
         Task Edit(Permiso permiso);
         Permiso GetById(int id);
    }
}
