﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.vb" Inherits="Examen.WebVB._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Permisos empleados</h1>
        <p class="lead">Administración de permisos de empleados</p>

    </div>
    <div class="row">

        <div class="col-md-12">


            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <hr />

                    <asp:FormView ID="FormView1" runat="server" DataSourceID="PermisosDataSource" DefaultMode="Insert">
                        <HeaderTemplate>
                            <h3>Nuevo Permiso</h3>
                            <hr />
                        </HeaderTemplate>
                        <InsertItemTemplate>
                            <div class="container">
                              
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" for="nombreEmpleado">Nombre</label>
                                            <asp:TextBox ID="NombreEmpleado" runat="server" CssClass="form-control" Text='<%# Bind("NombreEmpleado") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredNombreEmpleadoValidator" runat="server" ControlToValidate="NombreEmpleado" ErrorMessage="Debe ingresar Nombre" ForeColor="Maroon" ValidationGroup="PermisoNuevo"></asp:RequiredFieldValidator>

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" for="apellidoEmpleado">Apellido</label>
                                            <asp:TextBox ID="ApellidosEmpleado" runat="server" CssClass="form-control" Text='<%# Bind("ApellidosEmpleado") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredApellidosEmpleadoValidator" runat="server" ControlToValidate="ApellidosEmpleado" ErrorMessage="Debe ingresar Apellidos" ForeColor="Maroon" ValidationGroup="PermisoNuevo"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" for="tipoPermiso">Tipo Permiso</label>
                                            <asp:DropDownList ID="TipoPermisoId" runat="server" CssClass="form-control" DataSourceID="TipoPermisoDataSource" DataTextField="Descrpcion" DataValueField="Id" SelectedValue='<%# Bind("TipoPermisoId") %>'>
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label" for="FechaSolicitud">Fecha de Solicitud</label>
                                            <asp:TextBox ID="FechaPermiso" runat="server" ControlToValidate="FechaPermiso" CssClass="form-control" Text='<%# Bind("FechaPermiso", "{0:yyyy-MM-dd}") %>' TextMode="Date"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFechaValidator" runat="server" ControlToValidate="FechaPermiso" ForeColor="Maroon" ErrorMessage="Debe ingresar una fecha de solicitud" ValidationGroup="PermisoNuevo"></asp:RequiredFieldValidator>
                                           </div>
                                    </div>
                                    <div class="col-md-1">  
                                        <br />
                                           <asp:Button ID="NuevoPermiso" runat="server" CommandName="Insert" CssClass="btn btn-success" Text="Grabar" ValidationGroup="PermisoNuevo" />

                                </div>
                        </InsertItemTemplate>
                    </asp:FormView>


                    </div>


                    <hr />
                    <h3>Listado de permisos existentes</h3>
                    <hr />
                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" CssClass="table table-striped" DataSourceID="PermisosDataSource" ForeColor="#333333" GridLines="None" DataKeyNames="Id,NombreEmpleado,ApellidosEmpleado,FechaPermiso" PageSize="5">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:TemplateField HeaderText="Nombre" SortExpression="NombreEmpleado">
                                <EditItemTemplate>
                                    <asp:TextBox ID="NombreEmpleado" runat="server" CssClass="form-control" Text='<%# Bind("NombreEmpleado") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredNombreEmpleadoValidator" runat="server" ControlToValidate="NombreEmpleado" ErrorMessage="Debe ingresar Nombre" ForeColor="Maroon" ValidationGroup="PermisoDatos"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("NombreEmpleado") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Apellidos" SortExpression="ApellidosEmpleado">
                                <EditItemTemplate>
                                    <asp:TextBox ID="ApellidosEmpleado" runat="server" CssClass="form-control" Text='<%# Bind("ApellidosEmpleado") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredApellidosEmpleadoValidator" runat="server" ControlToValidate="ApellidosEmpleado" ErrorMessage="Debe ingresar Apellidos" ForeColor="Maroon" ValidationGroup="PermisoDatos"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("ApellidosEmpleado") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tipo Permiso" SortExpression="TipoPermiso">
                                <EditItemTemplate>
                                    <asp:DropDownList ID="TipoPermisoId" runat="server" CssClass="form-control" DataSourceID="TipoPermisoDataSource" DataTextField="Descrpcion" DataValueField="Id" SelectedValue='<%# Bind("TipoPermisoId") %>'>
                                    </asp:DropDownList>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("TipoPermiso.Descrpcion") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha de Solicitud" SortExpression="FechaPermiso">
                                <EditItemTemplate>
                                    <asp:TextBox ID="FechaPermiso" runat="server" ControlToValidate="FechaPermiso" CssClass="form-control" Text='<%# Bind("FechaPermiso", "{0:yyyy-MM-dd}") %>' TextMode="Date"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFechaValidator" runat="server" ControlToValidate="FechaPermiso" ForeColor="Maroon" ErrorMessage="Debe ingresar una fecha de solicitud"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("FechaPermiso", "{0:d}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <EditItemTemplate>
                                    <div aria-label="..." class="btn-group btn-xs" role="group">
                                        <asp:Button ID="ActualizarPermiso" runat="server" CommandName="Update" CssClass="btn btn-success" Text="Actualizar" ValidationGroup="PermisoDatos" />
                                        &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" CssClass="btn btn-warning" Text="Cancelar"></asp:LinkButton>
                                    </div>
                                    <asp:HiddenField ID="Id" runat="server" Value='<%# Bind("Id") %>' />
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <div aria-label="..." class="btn-group btn-xs" role="group">
                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" CssClass="btn btn-info" Text="Editar"></asp:LinkButton>
                                        &nbsp;<asp:LinkButton ID="EliminarPermiso" runat="server" CausesValidation="False" CommandName="Delete" CssClass="btn btn-danger" Text="Eliminar" OnClientClick="return confirm('Desea eliminar este permiso?')" EnableTheming="False"></asp:LinkButton>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:ObjectDataSource ID="PermisosDataSource" runat="server" DataObjectTypeName="Examen.Data.Permiso" DeleteMethod="Delete" InsertMethod="Add" SelectMethod="GetAllAsync" TypeName="Examen.WebVB.Controllers.PermisosController" UpdateMethod="Edit">
                <DeleteParameters>
                    <asp:Parameter Name="id" Type="Int32" />
                </DeleteParameters>

            </asp:ObjectDataSource>


        </div>
        <div class="modal fade" tabindex="-1" role="dialog" id="deleteModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;class="modal-body">
        ste permiso?</p>
                            <input id="deleteId" type="hidden" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" id="deletePermiso">Aceptar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


    </div>

    <asp:ObjectDataSource ID="TipoPermisoDataSource" runat="server" SelectMethod="GetAllAsync" TypeName="Examen.WebVB.Controllers.TipoPermisosController"></asp:ObjectDataSource>

</asp:Content>
