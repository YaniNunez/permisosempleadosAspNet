﻿Imports System.Net
Imports System.Threading.Tasks
Imports System.Web.Http
Imports Examen.Data
Imports Examen.Services.Interfaces

Namespace Controllers
    Public Class PermisosController
        Inherits ApiController

        Private ReadOnly _permisoService As IPermisoService
        Private ReadOnly _tipoPermisosService As ITipoPermisosService
        Private ReadOnly _db As EmpresaContext

        Public Sub New()
            _db = New EmpresaContext()
            _permisoService = New PermisoService(_db)
            _tipoPermisosService = New TipoPermisosService(_db)
        End Sub

        <HttpGet>
        Public Function GetAllAsync() As IList(Of Permiso)
            Try
                Return _permisoService.List()
            Catch ex As Exception
                Throw New ArgumentException("Se produjo un error, detalles: ", ex.Message)
            End Try
        End Function

        <HttpGet>
        <Route("api/permisos/{id:int}")>
        Public Function GetByIdAsync(ByVal id As Integer) As Permiso
            Try
                Return _permisoService.GetById(id)
            Catch ex As Exception
                Throw New ArgumentException("Se produjo un error, detalles: ", ex.Message)
            End Try
        End Function

        <HttpPost>
        Public Async Function Add(ByVal permiso As Permiso) As Task
            Try
                Await _permisoService.Add(permiso)
            Catch ex As Exception
                Throw New ArgumentException("Se produjo un error, detalles: ", ex.Message)
            End Try
        End Function

        <HttpPut>
        Public Async Function Edit(ByVal permiso As Permiso) As Task
            Try
                Await _permisoService.Edit(permiso)
            Catch ex As Exception
                Throw New ArgumentException("Se produjo un error, detalles: ", ex.Message)
            End Try
        End Function

        <HttpDelete>
        Public Async Function Delete(ByVal permiso As Permiso) As Task
            Try
                Await _permisoService.Delete(permiso.Id)
            Catch ex As Exception
                Throw New ArgumentException("Se produjo un error, detalles: ", ex.Message)
            End Try
        End Function
    End Class

End Namespace