﻿Imports System.Net
Imports System.Web.Http
Imports Examen.Data
Imports Examen.Services.Interfaces

Namespace Controllers
    Public Class TipoPermisosController
        Inherits ApiController

        Private ReadOnly _tipoPermisosService As ITipoPermisosService
        Private ReadOnly _db As EmpresaContext

        Public Sub New()
            _db = New EmpresaContext()
            _tipoPermisosService = New TipoPermisosService(_db)
        End Sub

        <HttpGet>
        Public Function GetAllAsync() As IList(Of TipoPermiso)
            Try
                Return _tipoPermisosService.List()
            Catch ex As Exception
                Throw New ArgumentException("Se produjo un error, detalles: ", ex.Message)
            End Try
        End Function
    End Class

End Namespace