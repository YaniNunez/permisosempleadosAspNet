﻿Public Class PermisoViewModel
    Dim _Id As Integer
    Dim _NombreEmpleado As String
    Dim _ApellidosEmpleado As String
    Dim _FechaPermiso As Date
    Dim _TipoPermiso As String
    Public Property Id As Integer
        Get
            Return _Id
        End Get
        Set(value As Integer)
            _Id = value
        End Set
    End Property

    Public Property NombreEmpleado As String
        Get
            Return _NombreEmpleado
        End Get
        Set(value As String)
            _NombreEmpleado = value
        End Set
    End Property

    Public Property ApellidosEmpleado As String
        Get
            Return _ApellidosEmpleado
        End Get
        Set(value As String)
            _ApellidosEmpleado = value
        End Set
    End Property

    Public Property FechaPermiso As Date
        Get
            Return _FechaPermiso
        End Get
        Set(value As Date)
            _FechaPermiso = value
        End Set
    End Property

    Public Property TipoPermiso As String
        Get
            Return _TipoPermiso
        End Get
        Set(value As String)
            _TipoPermiso = value
        End Set
    End Property
End Class
